//
//  GraphicsCollectionViewCell.swift
//  JodApp
//
//  Created by Lucas Coiado Mota on 16/07/15.
//  Copyright (c) 2015 Lucas Coiado Mota. All rights reserved.
//

import UIKit
import Charts

class GraphicsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var mes: UILabel!
    @IBOutlet weak var grafico: PieChartView!
    
    
    
    
}
