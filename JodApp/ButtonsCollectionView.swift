//
//  ButtonsCollectionView.swift
//  JodApp
//
//  Created by Lucas Coiado Mota on 29/07/15.
//  Copyright (c) 2015 Lucas Coiado Mota. All rights reserved.
//

import UIKit

class ButtonsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var buttons: UILabel!
    
}
